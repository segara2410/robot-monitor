#include "udp_server.h"

UDPServer::UDPServer(int port)
{
	udp_manager_.Create();
	udp_manager_.Bind(7979);
	udp_manager_.SetNonBlocking(true);
}

UDPServer::~UDPServer()
{
	udp_manager_.Close();
}

int UDPServer::Receive(char *message, int message_length)
{
	return udp_manager_.Receive(message, message_length);
}