#pragma once

#include "ofxNetwork.h"

class UDPServer
{
private:

	ofxUDPManager udp_manager_;

public:

	UDPServer (int port);
	~UDPServer ();

	int Receive(char* message, int message_length);
};